import os
import sys
import subprocess

errorCode = 1

temp = "./temp/"
source = "./tests/"
program = "./imapcl"

def runTests(directory, tests) :
	for i in range(len(tests)) :
		test = tests[i]
		sourcePath = source + directory + "/"
		tempPath = temp + directory + "/"
		if not os.path.exists(tempPath):
		    os.makedirs(tempPath)
		runTest(program + " " + test[1], str(i), tempPath + test[0], test[2], test[0])		

def runTest(startCMD, count, tempPath, returnCode, name) :
	cmd = startCMD + " 2> " +  tempPath + ".err 1>" +  tempPath + ".out"
	result = subprocess.call(cmd, shell=True)
	if result != returnCode :
		print ("FAIL - TEST " + count + " " + name)
	else:
		print ("OK - TEST " + count + " " + name)

cmd = "make clean-part 2>&1 > ./makeClean.tmp"
test = subprocess.call(cmd, shell=True)
if test != 0 :
    print ("FAIL - make clean-part")
    sys.exit(errorCode)
print ("OK - make clean-part")

cmd = "make test-start 2>&1 > ./make.tmp"
test = subprocess.call(cmd, shell=True)
if test != 0 :
    print ("FAIL - make test-start")
    sys.exit(errorCode)
print ("OK - make test-start")

if not os.path.exists(temp):
    os.makedirs(temp)

paramsTests = [
	["ok-01", "imap.seznam.cz -a ./tests/succLogin.txt -o ./temp", 0],
	["bad_login", "imap.seznam.cz -a ./tests/badLogin.txt -o ./", 1],
	["bad_url", "ima.seznam.cz -a ./tests/succLogin.txt -o ./", 1],
	["bad_input_00", "", 1],
	["bad_input_01", "seznam.cz", 1],
	["bad_input_02", "seznam.cz -h", 1],
	["bad_input_03", "seznam.cz -C aaa", 1],
	["bad_input_04", "seznam.cz -T -C", 1],
	["bad_input_05", "seznam.cz -T -c", 1],
	["bad_input_06", "seznam.cz -a /dev/null -o ./", 1],
]

runTests("params", paramsTests)



