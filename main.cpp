/* 
 * File:   main.cpp
 * Author: Lukáš Černý
 *
 * Created on October 9, 2016, 1:50 PM
 */

#include <cstdlib>
#include <string.h>

#include "ParamsParser.h"
#include "Imap.h"

using namespace std;

int main(int argc, char** argv)
{

    try {
        /* Parse input params. */
        ParamsParser *parser = new ParamsParser;
        parser->parseParams(argc, argv);

        if (parser->isOk() == false) {
            wcerr << "Bad input params." << endl;
            return (EXIT_FAILURE);
        }

        if (argc == 2) {
            return (EXIT_SUCCESS);
        }

        Imap *imap = new Imap(parser);
        if (imap->createConnection() == false) {
            wcerr << "Create connection FAILED" << endl;
            return (EXIT_FAILURE);
        }

        if (imap->login() == false) {
            wcerr << "Login FAILED" << endl;
            return (EXIT_FAILURE);
        }

        if (imap->selectFloderName() == false) {
            wcerr << "Command SELECT " << parser->getVOutDir().c_str() << " FAILED" << endl;
            return (EXIT_FAILURE);
        }

        if (imap->downloadMessages() == false) {
            wcerr << "Download messages FAILED" << endl;
            return (EXIT_FAILURE);
        }

        string report = "Donwloaded " + to_string(imap->getCountOfDownloadedMessages());
        if (parser->n == true) {
            report += " new";
        }
        report += " messages from the box ";
        report += parser->getVMailBox();
        report += ".";
        cout << report << endl;
    } catch (exception e) {
        wcerr << "Program FAILED!!!" << endl;
        return (EXIT_FAILURE);
    }
    return 0;
}