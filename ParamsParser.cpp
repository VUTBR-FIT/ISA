/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ParamsParser.cpp
 * Author: user
 * 
 * Created on October 11, 2016, 6:45 PM
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "ParamsParser.h"

ParamsParser::ParamsParser()
{
    /* Flagy jednotilivých parametrů */
    port = false;
    t = false;
    certfile = false;
    certaddr = false;
    n = false;
    h = false;
    authFile = false;
    mailbox = false;
    outDir = false;
    address = false;

    //FIXME
    port = 0;

    error = EXIT_SUCCESS;
}

ParamsParser::~ParamsParser() { }

void ParamsParser::printHelp()
{
    cout << "$> imapcl server [-p PORT] [-T [-c CERT_FILE] [-C CERT_ADDR]] [-n] [-h] [-a AUTH_FILE] [-b MAILBOX] -o OUT_DIR" << endl;
    cout << "  SERVER       - povinný parametr (url/IP adresa serveru)" << endl;
    cout << "  -p PORT      - specifikace čísla portu na serveru (defaultně: 143/993 pro SSL/TLS)" << endl;
    cout << "  -T           - zapíná šifromavání " << endl;
    cout << "  -c CERT_FILE - nastaví cestu k souboru s certifikáty " << endl;
    cout << "  -C CERT_ADDR - nastaví cestu ke složce s certifikáty (defaultně: /etc/ssl/certs)" << endl;
    cout << "  -n           - zapne stahování pouze nepřečtených zpráv" << endl;
    cout << "  -h           - zapne stahování pouze hlaviček emailů" << endl;
    cout << "  -a AUTH_FILE - nastaví cestu k souboru s přihlašovacím jménem a heslem" << endl;
    cout << "  -b MAILBOX   - nastaví jméno složky na serveru, ze které se budou stahovat maily" << endl;
    cout << "  -o OUT_DIR   - nastaví cestu ke složce pro ukládání" << endl;
    cout << "  --help       - vypíše tuto nápovědu" << endl;
}

void ParamsParser::parseParams(int argc, char **argv)
{
    int state = 0;
    char *param;

    for (int i = 1; i < argc; i++) {
        param = argv[i];
        if (this->error != EXIT_SUCCESS) {
            wcerr << "Bad input params." << endl;
            throw new exception;
        };
        switch (state) {
            case 0:
                if (strcmp(param, "-p") == 0) {
                    if (this->port) this->error = EXIT_FAILURE;
                    this->port = true;
                    state = 1;
                } else if (strcmp(param, "-T") == 0) {
                    if (this->t) this->error = EXIT_FAILURE;
                    this->t = true;
                } else if (strcmp(param, "-c") == 0) {
                    if (this->certfile) this->error = EXIT_FAILURE;
                    this->certfile = true;
                    state = 2;
                } else if (strcmp(param, "-C") == 0) {
                    if (this->certaddr) this->error = EXIT_FAILURE;
                    this->certaddr = true;
                    state = 3;
                } else if (strcmp(param, "-n") == 0) {
                    if (this->n) this->error = EXIT_FAILURE;
                    this->n = true;
                } else if (strcmp(param, "-h") == 0) {
                    if (this->h) this->error = EXIT_FAILURE;
                    this->h = true;
                } else if (strcmp(param, "-a") == 0) {
                    if (this->authFile) this->error = EXIT_FAILURE;
                    this->authFile = true;
                    state = 6;
                } else if (strcmp(param, "-b") == 0) {
                    if (this->mailbox) this->error = EXIT_FAILURE;
                    this->mailbox = true;
                    state = 7;
                } else if (strcmp(param, "-o") == 0) {
                    if (this->outDir) this->error = EXIT_FAILURE;
                    this->outDir = true;
                    state = 8;
                } else if (strcmp(param, "--help") == 0) {
                    if (argc != 2) {
                        break;
                    }
                    this->printHelp();
                    return;
                } else {
                    if (this->address) this->error = EXIT_FAILURE;
                    this->address = true;
                    this->vAddress = param;
                }
                break;
            case 1: // PORT
                this->vPort = std::atoi(param);
                state = 0;
                break;
            case 2: // CERTFILE
                this->vCertFile = param;
                state = 0;
                break;
            case 3: // CERTADDR
                this->vCertAddr = param;
                state = 0;
                break;
            case 6: // AUTHFILE
                this->vAuthFile = param;
                state = 0;
                break;
            case 7: // MAILBOX
                this->vMailBox = param;
                state = 0;
                break;
            case 8: // OUTDIR
                this->vOutDir = param;
                if (strcmp(this->vOutDir.substr(this->vOutDir.length() - 1).c_str(), "/") != 0) {
                    this->vOutDir += "/";
                }
                state = 0;
                break;
        }
    }
    if (state != 0) {
        this->error = EXIT_FAILURE;
    }

    if (this->address == false || this->authFile == false || this->outDir == false) {
        this->error = EXIT_FAILURE;
    }

    if (this->t == false && (this->certfile == true || this->certaddr == true)) {
        this->error = EXIT_FAILURE;
    }

    if (this->error == EXIT_SUCCESS) {
        string line;
        string user = "username = ";
        string pass = "password = ";

        ifstream myFile;
        myFile.open(this->vAuthFile);
        if (!myFile.is_open()) {
            wcerr << "The opening the file FAILED" << endl;
            this->error = EXIT_FAILURE;
            return;
        }

        getline(myFile, line);
        if (line.find(user) == 0) {
            this->login = line.substr(user.length());
        } else this->error = EXIT_FAILURE;

        if (this->error == EXIT_SUCCESS) {
            getline(myFile, line);
            if (line.find(pass) == 0) {
                this->password = line.substr(pass.length());
            } else this->error = EXIT_FAILURE;

            if (this->error == EXIT_SUCCESS) {
                getline(myFile, line);
                if (!myFile.eof()) {
                    wcerr << "Bad structure auth file" << endl;
                    this->error = EXIT_FAILURE;
                }
            }
        }
    }
}

string ParamsParser::getVAddress()
{
    return (this->vAddress);
}

string ParamsParser::getVCertFile()
{
    return (this->vCertFile);
}

string ParamsParser::getVCertAddr()
{
    if (this->certaddr) {
        return (this->vCertAddr);
    } else {
        return ("/etc/ssl/certs/");
    }
}

string ParamsParser::getVMailBox()
{
    if (this->mailbox) {
        return (this->vMailBox);
    } else {
        return ("INBOX");
    }
}

string ParamsParser::getVOutDir()
{

    return (this->vOutDir);
}

int ParamsParser::getVPort()
{
    if (this->port == false) {
        if (this->t) {
            // imap4 protocol over TLS/SSL
            return (993);
        } else {
            return (143);
        }
    } else {
        return (this->port);
    }
}

string ParamsParser::getLogin()
{
    return (this->login);
}

string ParamsParser::getPassword()
{
    return (this->password);
}

bool ParamsParser::isOk()
{
    return (this->error == EXIT_SUCCESS);
}