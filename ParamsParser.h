/* 
 * File:   ParamsParser.h
 * Author: Lukáš Černý
 *
 * Created on October 11, 2016, 6:45 PM
 */

#ifndef PARAMSPARSER_H
#define PARAMSPARSER_H

#include <iostream>
#include <string>
#include <fstream>
#include <dirent.h>

using namespace std;

class ParamsParser {
public:

    /* Flagy jednotilivých parametrů */
    bool port;
    bool t;
    bool certfile;
    bool certaddr;
    bool n;
    bool h;
    bool authFile;
    bool mailbox;
    bool outDir;
    bool address;

    /**
     * Rozparsování zadaných parametrů
     * 
     * @param argc počet vstupních parametrů
     * @param argv pole vstupních parametrů
     */
    void parseParams(int argc, char **argv);

    /**
     * Ověří zda zadané parametry jsou v pořádku
     * 
     * @return boolean 
     */
    bool isOk(void);
    ParamsParser();
    virtual ~ParamsParser();

    /* GETTERY */
    /** 
     * Vrací URL/IP adresu serveru 
     * @return string
     */
    string getVAddress();
    
    /**
     * Vrací cestu k souboru s certifikáty
     * @return string
     */
    string getVCertFile();
    
    /**
     * Vrací cestu k složce s certifikáty
     * @default /etc/ssl/certs
     * @return string
     */
    string getVCertAddr();
    
    /**
     * Vrací jméno složky, ze které se načítají maily
     * @default INBOX
     * @return string
     */
    string getVMailBox();
    
    /**
     * Vrací cestu k složce, do které se budou ukládat stažené maily
     * @return string
     */
    string getVOutDir();
    
    /**
     * Vrací číslo portu pro připojení
     * @default 143 pro nezabezpečené připojení
     * @default 993 pro zabezpečené připojení
     * @return integer
     */
    int getVPort();
    
    
    /**
     * Vrací username ze souboru zadaného přepínačem -a FILE
     * @return string
     */
    string getLogin();
    
    /**
     * Vrací password ze souboru zadaného přepínačem -a FILE
     * @return string
     */
    string getPassword();
private:
    int error;

    /* Hodnoty načtených parametrů */
    string vAddress;
    int vPort;
    string vCertFile;
    string vCertAddr;
    string vAuthFile;
    string vMailBox;
    string vOutDir;
    string login;
    string password;
    
    /**
     * Vypíše nápovědu na STDOUT
     */
    void printHelp(void);
};

#endif /* PARAMSPARSER_H */

