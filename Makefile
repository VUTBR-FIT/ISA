# Environment 
CC=g++
FLAGS=-std=c++11 -w -O3
OUT=imapcl
RM=rm -fr
TEMP=./temp/
TAR=xcerny63.tar
#-----------------------------------------------------------------

all: main.o ParamsParser.o Imap.o
	$(CC) main.o ParamsParser.o Imap.o -o $(OUT)  -lcrypto -lssl


# UNIVERSAL RULE
%.o : %.cpp
	$(CC) $(FLAGS) -c $< -o $@

# CLEAN RULES
clean: clean-part
	$(RM) *.temp
clean-part:
	$(RM) *.list
	$(RM) temp/*
	$(RM) $(OUT)
	$(RM) *.o
	$(RM) *.tmp
	$(RM) $(TAR)
	
# DEPENDENS GENERATOR
dep:
	$(CC) $(FLAGS) -MM *.cpp >dep.list
-include dep.list

# TAR
tar: clean
	tar -cvf xcerny63.tar *.cpp *.h Makefile *.pdf README*


# TESTS
test: clean
	python ./tests/test.py

test-start: FLAGS=-std=c++11 -O3 -Wall -Wextra -Werror -Wno-unused-value -pedantic -pedantic-errors -g
test-start: all
