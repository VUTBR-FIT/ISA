/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Imap.cpp
 * Author: Lukáš Černý
 * 
 * Created on October 13, 2016, 1:57 PM
 */

#include <sys/socket.h>
#include "ParamsParser.h"
#include "Imap.h"

Imap::Imap(ParamsParser *parser)
{
    // Inicializace OpenSSL
    SSL_load_error_strings();
    ERR_load_BIO_strings();
    OpenSSL_add_all_algorithms();
    SSL_library_init();

    this->setParams(parser);

    this->id_request = 0;
    this->connection = NULL;
    this->coutOfMessages = 0;
    this->donwloadedMessages = 0;
}

Imap::~Imap()
{
    this->closeConnection();
}

void Imap::setParams(ParamsParser *params)
{
    this->params = params;
}

bool Imap::createConnection()
{
    if (this->params->t == true) {
        return (this->createSecuredConnection());
    } else {
        return (this->createNotSecuredConnection());
    }

}

bool Imap::createNotSecuredConnection()
{
    string temp = this->params->getVAddress();
    temp += ":";
    temp += to_string(this->params->getVPort());

    char *tempChar = new char[temp.length() + 1];
    strcpy(tempChar, temp.c_str());

    char *temp2 = new char[this->params->getVOutDir().length() + 1];
    memcpy(temp2, this->params->getVOutDir().c_str(), this->params->getVOutDir().length());
    temp2[this->params->getVOutDir().length()] = '\0';

    DIR *dir = opendir(temp2);
    if (dir) {
        closedir(dir);
    } else if (ENOENT == errno) {
        wcerr << "Directory (" << this->params->getVOutDir().c_str() << ") not exists." << endl;
        return (false);
    } else {
        wcerr << "Opening the directory (" << this->params->getVOutDir().c_str() << ") failed" << endl;
        return (false);
    }
    delete [] temp2;


    // Creating and opening a connection
    this->connection = BIO_new_connect(tempChar);

    delete [] tempChar;

    if (this->connection == NULL) {
        wcerr << "Creating the not secured connection failed in BIO_new_connext()." << endl;
        return (false);
    }
    if (BIO_do_connect(this->connection) <= 0) {
        wcerr << "Creating the not secured connection failed in BIO_do_connext()." << endl;
        return (false);
    }

    this->getResponse();
    if (this->data.find("* OK ") <= 1) {
        return (true);
    } else {
        return (false);
    }
}

bool Imap::createSecuredConnection()
{
    // Setting up the SSL pointers
    this->ctx = SSL_CTX_new(SSLv23_client_method());

    if (this->params->certfile == true) {
        // Loading a trust store
        if (!SSL_CTX_load_verify_locations(this->ctx, this->params->getVCertFile().c_str(), NULL)) {
            wcerr << "Creating the secured connection failed in SSL_CTX_load_verify_locations(FILE)." << endl;
            return (false);
        }
    }

    DIR *dir = opendir(this->params->getVOutDir().c_str());
    if (dir) {
        closedir(dir);
    } else if (ENOENT == errno) {
        wcerr << "Directory (" << this->params->getVOutDir().c_str() << ") not exists." << endl;
        return (false);
    } else {
        wcerr << "Opening the directory (" << this->params->getVOutDir().c_str() << ") failed" << endl;
        return (false);
    }

    if (this->params->certaddr || !this->params->certfile) {
        // Preparing a certificate folder and using it
        /* Tento kod je zakomentovany protoze na serveru merlin nefunguje
        string cmd = "c_rehash " + this->params->getVOutDir() + " > /dev/null";

        if (system(cmd.c_str()) != EXIT_SUCCESS) {
            wcerr << "Command (" << cmd.c_str() << ") FAILED" << endl;
            return (false);
        }*/

        if (!SSL_CTX_load_verify_locations(this->ctx, NULL, this->params->getVCertAddr().c_str())) {
            wcerr << "Creating the secured connection failed in SSL_CTX_load_verify_locations(DIR)." << endl;
            return (false);
        }
    }

    // Setting up the BIO object
    this->connection = BIO_new_ssl_connect(this->ctx);
    BIO_get_ssl(this->connection, & (this->ssl));
    SSL_set_mode(this->ssl, SSL_MODE_AUTO_RETRY);

    // Opening a secure connection
    string hostname = this->params->getVAddress() + ":" + to_string(this->params->getVPort());

    /* Attempt to connect */
    BIO_set_conn_hostname(this->connection, hostname.c_str());

    /* Verify the connection opened and perform the handshake */
    if (BIO_do_connect(this->connection) <= 0) {
        wcerr << "BIO_do_connect() FAILED" << endl;
        return (false);
    }

    // Checking if a certificate is valid
    if (SSL_get_verify_result(this->ssl) != X509_V_OK) {
        wcerr << "SSL verification FAILED" << endl;
        return (false);
    }

    return (true);
}

bool Imap::login()
{
    string command = "LOGIN " + this->params->getLogin() + " " + this->params->getPassword();

    if (this->sendRequest(command)) {
        if (this->getResponse()) {
            if (this->data.find(std::to_string(this->id_request) + " OK ") != string::npos) {
                return (true);
            }
        }
    }
    return (false);
}

bool Imap::selectFloderName()
{
    string command = "SELECT " + this->params->getVMailBox();

    if (this->sendRequest(command)) {
        if (this->getResponse()) {
            if (this->data.find(std::to_string(this->id_request) + " OK ") != string::npos) {
                // ziskani poctu zprav v emailu

                istringstream tempStream(this->data);
                string tempLine;

                while (getline(tempStream, tempLine)) {
                    if (tempLine.find(" EXISTS\r") != string::npos) {
                        this->coutOfMessages = stoi(tempLine.substr(3, tempLine.find(" EXISTS\r") - 3));
                        break;
                    }
                }
                return (true);
            }
        }
    }
    return (false);
}

bool Imap::downloadMessages()
{
    char *tempChar = new char[this->params->getVOutDir().length() + 1];
    strcpy(tempChar, this->params->getVOutDir().c_str());
    DIR *directory = opendir(tempChar);
    if (directory) {
        // Slozka byla uspesne otevrena
        closedir(directory);
    } else if (ENOENT == errno) {
        wcerr << "Opening the directory (" << this->params->getVOutDir().c_str() << ") FAILED (not exists)." << endl;
        return (false);
    } else {
        wcerr << "Opening the directory (" << this->params->getVOutDir().c_str() << ") FAILED (unknown reason)." << endl;
        return (false);
    }
    ofstream file;
    string tempPath;
    string firstLine;
    string startFirstLine;
    int lengthMessage;
    for (int i = 1; i <= this->coutOfMessages; i++) {
        if (this->params->n == true) {
            if (this->sendRequest("FETCH " + to_string(i) + " FLAGS") == false) {
                return (false);
            }
            if (this->getResponse() == false) {
                wcerr << "Command FETCH XX FLAGS FAILED" << endl;
            }
            if (this->getData().find("\\Seen") != string::npos) {
                continue;
            }
        }
        if (this->params->h == true) {
            if (this->sendRequest("FETCH " + to_string(i) + " RFC822.HEADER") == false) {
                return (false);
            }
        } else {
            if (this->sendRequest("FETCH " + to_string(i) + " RFC822") == false) {
                return (false);
            }
        }
        if (this->getResponse() == false) {
            wcerr << "Command FETCH XX RFC822.? FAILED" << endl;
        }
        this->donwloadedMessages++;
        firstLine = this->data.substr(0, this->data.find_first_of("\r\n"));
        if (this->params->h == true) {
            startFirstLine = " * " + to_string(i) + " FETCH (RFC822.HEADER {";
        } else {
            startFirstLine = " * " + to_string(i) + " FETCH (RFC822 {";
        }

        tempPath = this->params->getVOutDir().c_str() + to_string(i) + ".txt";
        lengthMessage = atoi(firstLine.substr(startFirstLine.length(), firstLine.length() - startFirstLine.length() - 1).c_str());

        file.open(tempPath.c_str());
        file << this->data.substr(firstLine.length() + 2, lengthMessage);
        file.close();
    }
    return (true);
}

bool Imap::sendRequest(string request)
{
    this->id_request++;
    string newRequest = std::to_string(this->id_request) + " " + request + "\r\n";

    char *temp = new char[newRequest.length()];
    strcpy(temp, newRequest.c_str());

    if (BIO_write(this->connection, temp, newRequest.length()) <= 0) {
        if (!BIO_should_retry(this->connection)) {
            delete [] temp;
            return (false);
        }
    }
    return (true);
}

bool Imap::getResponse()
{
    this->data = '\0';
    int initTemp = 200;
    char *temp;
    bool whileOn = true;
    bool endLine = false;
    int posEndLine;


    while (whileOn || !endLine) {
        // Vytvorim doscasnou promennou pro ulozeni zpravy ze serveru
        temp = new char[initTemp + 1];
        int x = BIO_read(this->connection, temp, initTemp - 1);

        if (x == 0) {
            wcerr << "Connection closed." << endl;
            delete [] temp;
            return (false);
        } else if (x < 0) {
            if (!BIO_should_retry(this->connection)) {
                wcerr << "Read failed" << endl;
                delete [] temp;
                return (false);
            }
        }

        for (int i = 0; i < x; i++) {
            this->data += temp[i];
        }


        if (this->id_request != 0) {
            // Odpoved s vice radky
            posEndLine = this->data.find(to_string(this->id_request) + " OK ");
            if (posEndLine == ((int) string::npos)) {
                posEndLine = this->data.find(to_string(this->id_request) + " NO ");
            }

            if (posEndLine != ((int) string::npos) && posEndLine <= 1) {
                // Odpoved s jednim radkem
                if (this->data.find("\r\n") != string::npos) {
                    // Ukoncovaci radek je cely
                    whileOn = false;
                    endLine = true;
                    break;
                } else {
                    // Ukoncovaci radek neni cely
                    endLine = true;
                }
            } else if (posEndLine != ((int) string::npos)) {
                posEndLine = this->data.find("\r\n" + to_string(this->id_request) + " OK");
                if (this->data.find("\r\n" + to_string(this->id_request) + " OK") == string::npos) {
                    posEndLine = this->data.find("\r\n" + to_string(this->id_request) + " NO");
                }
                if (this->data.find_last_of("\r\n") != string::npos && ((int) this->data.find_last_of("\r\n")) > posEndLine) {
                    // Ukoncovaci radek je cely
                    whileOn = false;
                    endLine = true;
                    break;
                } else {
                    // Ukoncovaci radek neni cely
                    endLine = true;
                }
            }
        } else {
            if (this->data.find("* ") <= 1) {
                if (this->data.find('\r') != string::npos) {
                    whileOn = false;
                    endLine = true;
                    break;
                } else {
                    endLine = true;
                }
            }
        }
        delete [] temp;
        temp = NULL;
    }
    return (true);
}

bool Imap::closeConnection()
{
    if (this->connection != NULL) {
        BIO_reset(this->connection);
        BIO_free_all(this->connection);
        if (this->params->t == true) {
            SSL_CTX_free(this->ctx);
        }
        this->connection = NULL;
    }
    return (true);
}

string Imap::getData()
{
    string temp = this->data;
    this->data = "";
    return (temp);
}

int Imap::getCountOfDownloadedMessages()
{
    return (this->donwloadedMessages);
}