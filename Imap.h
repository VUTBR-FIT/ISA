/* 
 * File:   Imap.h
 * Author: Lukáš Černý
 *
 * Created on October 13, 2016, 1:57 PM
 */




#ifndef IMAP_H
#define IMAP_H

#include "ParamsParser.h"
#include "openssl/bio.h"
#include "openssl/ssl.h"
#include "openssl/err.h"
#include <sstream>

using namespace std;

class Imap {
public:
    Imap(ParamsParser *parser);
    virtual ~Imap();

    /**
     * Nastaví objekt s nastavením
     * @param params Objekt s nastavením/vstupními parametry
     */
    void setParams(ParamsParser *params);

    /**
     * Vytvoří připojení k serveru
     * 
     * @return bool Úspěch operace
     */
    bool createConnection();

    /**
     * Ukončí připojení k serveru
     * 
     * @return bool Úspěch operace
     */
    bool closeConnection();

    /**
     * Pošle požadavek na server
     * @return Úspěch uperace
     */
    bool getResponse();

    /**
     * Získá celou odpoěď ze serveru
     * @param request
     * @return 
     */
    bool sendRequest(string request);

    /**
     * Vátí získaná data ze serveru
     * @return string
     */
    string getData();

    /**
     * Provede přihlášení
     * @return Úspěch operace
     */
    bool login();

    /**
     * Nastavení názvu složky, se kterou se bude pracovat
     * @return Úspěch operace
     */
    bool selectFloderName();


    /**
     * Stáhne emaily
     * @return Úspěch operace
     */
    bool downloadMessages();

    /**
     * Vrátí počet stažených zpráv
     * @return int Počet stažených zpráv
     */
    int getCountOfDownloadedMessages();


private:
    int id_request;
    string data;
    ParamsParser *params;
    BIO *connection;
    SSL * ssl;
    SSL_CTX * ctx;

    /**
     * V této funkci jsem použil princip inicializace komunikace z webu
     * http://www.ibm.com/developerworks/library/l-openssl/
     * 
     * @author Lukáš Černý
     * @author Kenneth Ballard
     * @return Úspěch operace
     */
    bool createSecuredConnection();

    /**
     * V této funkci jsem použil princip inicializace komunikace z webu
     * http://www.ibm.com/developerworks/library/l-openssl/
     * 
     * @author Lukáš Černý
     * @author Kenneth Ballard
     * @return Úspěch operace
     */
    bool createNotSecuredConnection();
    string BUFFER;
    int coutOfMessages;
    int donwloadedMessages;
};

#endif /* IMAP_H */

